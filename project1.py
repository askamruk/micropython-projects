from microbit import *
import radio

my_images = [Image.HEART, Image.HEART_SMALL, Image.HAPPY, Image.SMILE,
            Image.SAD, Image.CONFUSED, Image.ANGRY, Image.ASLEEP, 
            Image.SURPRISED, Image.SILLY, Image.FABULOUS, Image.MEH, 
            Image.YES, Image.NO]

nr = 0
radio.on()

display.show(Image.YES)

while True:
    nr_str = radio.receive()
    if nr_str:
        try:
            nr = int(nr_str)
            display.show(my_images[nr])
        except ValueError:
            display.scroll(nr_str)
        except:
            pass
    if button_a.is_pressed():
        nr += 1
        nr = nr % 14
        display.show(my_images[nr])
        sleep(500)
    if button_b.is_pressed():
        radio.send(str(nr))
        sleep(500)