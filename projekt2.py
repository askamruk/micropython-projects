import speech
from microbit import sleep

speech.say("I can sing!")
sleep(1000)
speech.say("Listen to me!")
sleep(1000)


speech.pronounce("AEAE/HAEMM", pitch=200, speed=100)  # Ahem
sleep(1000)

solfa = [
    "#115DOWWWWWW",   # Doh
    "#103REYYYYYY",   # Re
    "#94MIYYYYYY",    # Mi
    "#88FAOAOAOAOR",  # Fa
    "#78SOHWWWWW",    # Soh
    "#70LAOAOAOAOR",  # La
    "#62TIYYYYYY",    # Ti
    "#58DOWWWWWW",    # Doh
]

song = ''.join(solfa)
speech.sing(song, speed=100)
solfa.reverse()
song = ''.join(solfa)
speech.sing(song, speed=100)