import radio
import random
from microbit import display, Image, button_a, sleep

very_small_square = Image(
             "00000:"
             "00000:"
             "00900:"
             "00000:"
             "00000")

small_square = Image(
             "00000:"
             "09990:"
             "09990:"
             "09990:"
             "00000")

square = Image(
             "99999:"
             "99999:"
             "99999:"
             "99999:"
             "99999")

radio.on()

while True:
    display.show([very_small_square, small_square, square], delay=200)
    
    if button_a.was_pressed():
        radio.send('flash')

    incoming = radio.receive()
    
    if incoming == 'flash':
        sleep(random.randint(50, 350))
        display.show(Image.HAPPY)
        sleep(random.randint(1, 10)*1000)